using AFramework;
using System;
using System.Collections;

public class ConfigDataManager : SingletonMono<ConfigDataManager>
{
    public bool isDoneInit { get; set; }

    public void Init(Action callback)
    {
        StartCoroutine(ProgressLoadConfig(callback));
    }

    private IEnumerator ProgressLoadConfig(Action callback)
    {
        if (!isDoneInit)
        {

        }

        yield return null;

        callback?.Invoke();
    }

}
