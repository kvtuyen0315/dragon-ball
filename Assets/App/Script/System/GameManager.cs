using AFramework;
using AFramework.UI;
using System.Collections;
using UnityEngine;

public class GameManager : SingletonMono<GameManager>
{
    private void Start()
    {
#if UNITY_EDITOR
        Application.runInBackground = true;
#endif
        Application.targetFrameRate = 60;

        CanvasManager.Init(GlobalInfo.UIDefaultPath, GlobalInfo.UIFlashScreenPopup);

        ConfigDataManager.I.Init(() =>
        {
            GameData.I.RegisterSaveData();
            SaveGameManager.I.Load();

            AudioManager.I.PlayMusic(MusicIndex.MAIN);
            CanvasManager.Push(GlobalInfo.UIMainMenu, null);

            StartCoroutine(CR_AutoSave());
        });

    }

    private IEnumerator CR_AutoSave()
    {
        WaitForSeconds wait = new WaitForSeconds(5);
        while (true)
        {
            yield return wait;
            SaveGameManager.I.Save();
        }

    }

}
