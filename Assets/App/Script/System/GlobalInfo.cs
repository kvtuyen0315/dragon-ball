using AFramework;

public class GlobalInfo : SingletonMono<GlobalInfo>
{
    #region Name menu & popup.
    public const string UIDefaultPath = "UI/";

    // Menu.
    public const string UIMainMenu = "Menu/MainMenu";

    // Popup.
    public const string UIFlashScreenPopup = "Popup/FlashScreenPopup";
    #endregion
}
