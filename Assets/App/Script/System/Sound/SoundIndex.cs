﻿public enum MenuSoundIndex
{
	None = -1,
	BACK,
	CLICK,
}

public enum MusicIndex
{
	None = -1,
	MAIN,
	IN_GAME,
}