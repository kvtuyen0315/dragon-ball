using AFramework.UI;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : BaseUIMenu
{
    [SerializeField] Button buttonStartGame;

    private void Awake()
    {
        buttonStartGame.onClick.AddListener(() =>
        {
            AudioManager.I.PlaySound(MenuSoundIndex.CLICK);

#if UNITY_EDITOR
            Debug.LogError("Start Game!!!");
#endif

        });
    }

    public override void Init(object[] initParams)
    {
        base.Init(initParams);

    }
}
