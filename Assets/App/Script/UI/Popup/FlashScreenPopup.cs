using AFramework.UI;

public class FlashScreenPopup : BaseUIMenu
{
    public override void Init(object[] initParams)
    {
        base.Init(initParams);

        CanvasManager.PopSelf(this, true);
    }
}
